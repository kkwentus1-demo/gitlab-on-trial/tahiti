import sys

import dj_database_url

from .common import *

print('[INFO] Importing image settings using postgres', file=sys.stderr)
SECRET_KEY = 'django-insecure-%q#z!x!5!44(t2h1)o55!&igl$-xxpjugh+$lt5t5n@7ppkcm3'

DEBUG = True
STATIC_WSGI = True

# This looks insecure!
ALLOWED_HOSTS = ['*']

# Only works in production, and then probably too open
USE_X_FORWARDED_HOST = True
CSRF_TRUSTED_ORIGINS = ["https://*.ilovedevops.com"]


# Requires DATABASE_URL

DATABASES = {
    "default": dj_database_url.config()
}
