# Sample application for Auto Devops POC

## Desktop

Run the app on a desktop (MacOS) using Python 3.11.


```bash
dev/desktop/init
source .venv/bin/activate
dev/desktop/poetry
dev/desktop/clean
dev/desktop/provision
dev/desktop/start
```

## Docker compose

Run local containers with docker compose (Requires Docker Desktop)

```bash
dev/compose/clean
dev/compose/provision
```

Troubleshoot the database

```bash
docker run -it postgres psql -U postgres
```

