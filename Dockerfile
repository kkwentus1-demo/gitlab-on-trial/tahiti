FROM python:3.11 AS dependencies

RUN pip install --upgrade poetry && \
    poetry config virtualenvs.create false

COPY ./pyproject.toml ./poetry.lock /

RUN poetry install --no-interaction --no-root && \
    rm -rf /root/.cache/pypoetry && \
    rm ./pyproject.toml ./poetry.lock

FROM dependencies as app

COPY ./ app

WORKDIR /app

ENTRYPOINT ["gunicorn", "pocsite.wsgi", "--bind", "0.0.0.0:8002"]

